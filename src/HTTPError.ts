export class HTTPError extends Error {
  constructor(message: string, readonly status = 500) {
    super(status + " " + message);
  }
}
