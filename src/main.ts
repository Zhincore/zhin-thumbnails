import { ServeOptions } from "bun";
import { loadConfig, getHostConfig } from "./Config";
import { ThumbnailOptions, Thumbnailer } from "./thumbnailer";
import { HTTPError } from "./HTTPError";
import Path from "node:path";

const config = await loadConfig();
const thumbnailer = new Thumbnailer();

export default {
  async fetch(request) {
    const url = new URL(request.url);
    const pathComps = url.pathname.slice(1).split("/");

    // Find host
    let host = "";

    if (config.routing == "path") {
      host = pathComps.shift() ?? "";
    } else {
      host = url.host.split(":")[0].split(".")[0];
    }

    // Load host config
    const hostConfig = getHostConfig(host, config);

    if (!hostConfig) return new Response("Host not found", { status: 404 });

    // Get opts
    const filename = pathComps.pop() ?? "";
    const extname = Path.extname(filename);
    const format = extname.slice(1);
    if (!format) return new Response("Missing file extension", { status: 404 });
    pathComps.push(filename.slice(0, -extname.length));

    const size = Number(pathComps.shift() ?? "");
    if (!size) return new Response("Invalid size", { status: 404 });

    const animated = url.searchParams.get("animated") != "false";

    const opts: ThumbnailOptions = {
      format,
      size,
      animated,
    };

    // Acquire the thumbnail
    try {
      const path = decodeURIComponent(pathComps.join("/"));
      const thumbnail = await thumbnailer.getThumbnail(path, opts, hostConfig, request.signal);
      if (!thumbnail) return new Response("File not found", { status: 404 });

      return new Response(Bun.file(thumbnail));
    } catch (err) {
      if (err instanceof HTTPError) {
        // Validation error
        return new Response(err.toString(), { status: err.status });
      }

      // Unexpected error
      console.trace(err);
      return new Response("Internal server error", { status: 500 });
    }
  },
} satisfies ServeOptions;
