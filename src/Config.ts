import YAML from "yaml";

export interface HostConfig {
  widths: number[];
  formats: string[];
  forbiddenFormats: string[];
  fit: "inside" | "outside";
  path: string;
  cachePath: string;
  timeout: number;
}

export interface Config {
  routing: "path" | "domain";
  defaults: HostConfig;
  hosts: Record<string, Partial<HostConfig>>;
}

export async function loadConfig() {
  const config = YAML.parse(await Bun.file("config.yaml").text());
  return config as unknown as Config;
}

export function getHostConfig(host: string, config: Config): HostConfig | undefined {
  if (!(host in config.hosts)) return;
  return Object.assign({}, config.defaults, config.hosts[host]);
}
