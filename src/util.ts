export function throwIfAborted(signal?: AbortSignal) {
  if (signal && signal.aborted) throw new Error("Aborted", signal.reason);
}

export function slugifyFilename(path: string) {
  return path.replace(/\/|%/g, "-").replace(/-{2,}/g, "-").replace(/^-/, "");
}
