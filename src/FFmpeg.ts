import { spawn } from "node:child_process";
import type { ChildProcess } from "node:child_process";
import { cpus } from "node:os";
import Limit from "p-limit";

export class FFmpeg {
  readonly #ffmpegs = new Set<ChildProcess>();
  readonly #limit = Limit(Math.max(1, Math.floor(cpus().length / 3)));

  async createThumbnail(size: number, input: string, output: string) {
    return this.#limit(this.#runFFmpeg.bind(this), size, input, output);
  }

  async #runFFmpeg(size: number, input: string, output: string) {
    const ffmpeg = spawn("ffmpeg", [
      "-v",
      "error",
      "-i",
      input,
      "-map",
      "v:0",
      "-vf",
      `mpdecimate,scale=${size}:${size}:force_original_aspect_ratio=increase`,
      "-frames",
      "1",
      output,
    ]);
    this.#ffmpegs.add(ffmpeg);

    let err = "";
    ffmpeg.stderr.on("data", (d) => (err += String(d)));

    return new Promise<void>((resolve, reject) => {
      ffmpeg.once("exit", (code) => {
        this.#ffmpegs.delete(ffmpeg);

        if (!code) return resolve();
        reject(new FFmpegError("FFmpeg exitted with non-null exit code.", err));
      });
    });
  }

  killAll() {
    for (const ffmpeg of this.#ffmpegs) {
      ffmpeg.kill(9);
    }
  }
}

class FFmpegError extends Error {
  constructor(message: string, readonly output?: string) {
    super(message);
  }

  toString() {
    return super.toString() + JSON.stringify({ output: this.output });
  }
}
