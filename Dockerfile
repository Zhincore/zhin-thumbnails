FROM oven/bun:0.6

WORKDIR /app

# Install ffmpeg # TODO: Doesn't work
RUN apt update
RUN apt install ffmpeg curl -y

# Copy app
COPY . .

# Install app dependencies
RUN bun install -p --frozen-lockfile

# Install node, cuz... sharp needs it
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
RUN apt install -y nodejs

# Install Sharp
RUN npm i sharp --no-save --frozen-lockfile

# Remove node
RUN apt remove -y nodejs

EXPOSE 3000
CMD [ "bun", "./src/main.ts" ]
