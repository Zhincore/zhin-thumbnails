# Zhincore's Thumbnail Service

Fast service for creating thumbnails for images and videos (if FFmpeg is available).

Powered by [Sharp](https://sharp.pixelplumbing.com/).

## Requirements

This project needs the following software installed:

- [Bun](https://bun.sh/) - the fast all-in-one JavaScript runtime. (tested in v0.6.12)
- (optionally) [FFmpeg](https://ffmpeg.org) - for thumbnailing videos and some unsupported formats

## Installation

1. Install dependencies using `bun install`
2. Copy `config.example.yaml` to `config.yaml` and modify for your needs.
3. Run using `bun src/main.ts`

